﻿using Medefer.PhotoGallery.Repository;
using Medefer.PhotoGallery.Repository.HttpClientFactory;
using Microsoft.Extensions.DependencyInjection;

namespace Medefer.PhotoGallery.Client.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddPhotoGalleryClient(this IServiceCollection services)
        {
            services.AddSingleton<IHttpClientFactory, HttpClientFactory>();
            services.AddSingleton<IPhotosGallery, PhotosGallery>();
            return services;
        }
    }
}
