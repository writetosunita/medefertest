﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Medefer.PhotoGallery.Api.ViewModel
{
    public class PhotoAlbum
    {
        public int UserId { get; set; }
        //public int Id { get; set; }
        public string AlbumTitle { get; set; }


        public int AlbumId { get; set; }
        public int PhotoId { get; set; }
        public string PhotoTitle { get; set; }
        public string Url { get; set; }
        public string ThumbnailUrl { get; set; }
    }
}
