﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Medefer.PhotoGallery.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;

namespace Medefer.PhotoGallery.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotoGalleryController : ControllerBase
    {
        private IPhotosGallery _photosGallery;

        public PhotoGalleryController(IPhotosGallery photosGallery)
        {
            _photosGallery = photosGallery;
        }

        // GET api/values
        [HttpGet]
        public async Task<HttpResponseMessage> GetPhotoAlbums()
        {
            var photostTask =  _photosGallery.GetPhotos();
            var albumsTask =  _photosGallery.GetAlbum();

            await Task.WhenAll(photostTask, albumsTask); //WhenAll to combine the output from the API

            var photos = await photostTask;
            var albums = await albumsTask;

            var photoalbums = albums.Join(photos, album => album.Id, photo => photo.AlbumId, (album, photo) => new
            {
                UserId = album.Id,
                AlbumTitle = album.Title,
                AlbumId = photo.AlbumId,
                PhotoId = photo.Id,
                PhotoTitle = photo.Title,
                Url = photo.Url,
                ThumbnailUrl = photo.ThumbnailUrl
            });

            return photoalbums as HttpResponseMessage;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetByUserId(int id)
        {

            return null;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetByUserName(int id)
        {
            return null;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetByPhtoName(int id)
        {
            return null;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetByPhotoId(int id)
        {
            return null;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetByAlbumId(int id)
        {
            return null;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetByAlbumName(int id)
        {
            return null;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
