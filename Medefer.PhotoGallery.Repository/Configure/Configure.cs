﻿namespace Medefer.PhotoGallery.Repository.Configure
{
    public static class Configure
    {
        public static string BaseUrl { get; set; } = "http://jsonplaceholder.typicode.com";
        public static string MethodName { get; set; }
    } 
}
