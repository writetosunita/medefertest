﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Medefer.PhotoGallery.Repository.HttpClientFactory
{
    public interface IHttpClientFactory
    {
        Func<Task<HttpResponseMessage>> Get(string action);
    }

    public class HttpClientFactory : IHttpClientFactory
    {
      
        private static readonly object LockHttpClient = new object();

        private static  HttpClient _httpClient;
        

        public static HttpClient Client
        {
            get
            {
                if (_httpClient == null)
                {
                    lock (LockHttpClient)
                    {
                        if (_httpClient == null)
                        {
                            _httpClient = new HttpClient();
                        }
                    }
                }

                return _httpClient;
            }
        }

        public Func<Task<HttpResponseMessage>> Get(string action)
        {  
            Client.BaseAddress = new Uri(Configure.Configure.BaseUrl);

            return () => Client.GetAsync(action);
        }
    }
}
