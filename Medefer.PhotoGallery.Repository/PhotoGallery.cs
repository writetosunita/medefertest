﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Medefer.PhotoGallery.Model.Entities;
using Medefer.PhotoGallery.Repository.HttpClientFactory;


namespace Medefer.PhotoGallery.Repository
{
    public class PhotosGallery : IPhotosGallery
    {
        private readonly HttpClientFactory.IHttpClientFactory _factory;

        public PhotosGallery(IHttpClientFactory factory)
        {
            _factory = factory;
        }

        public async Task<List<Photos>> GetPhotos()
        {
            var getPhotos = _factory.Get("photos");
            var photosResponse = (await getPhotos()).Content;
            var photosObjects = await photosResponse.ReadAsync()<List<Photos>>(); //extension method from webapiclient
            return photosObjects;
        }

        public async Task<List<Albums>> GetAlbum()
        {
            var getAlbums = _factory.Get("albums");
            var albumsResponse = (await getAlbums()).Content;
            var albumsObjects = await albumsResponse.ReadAsync()<List<Albums>>(); //extension method from webapiclient
            return albumsObjects;
  
        }
    }
}
