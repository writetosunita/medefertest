﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Medefer.PhotoGallery.Model.Entities;

namespace Medefer.PhotoGallery.Repository
{
    public interface IPhotosGallery
    {
        Task<List<Photos>> GetPhotos();
        Task<List<Albums>> GetAlbum();
    }
}